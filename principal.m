function varargout = principal(varargin)
% PRINCIPAL MATLAB code for principal.fig
%      PRINCIPAL, by itself, creates a new PRINCIPAL or raises the existing
%      singleton*.
%
%      H = PRINCIPAL returns the handle to a new PRINCIPAL or the handle to
%      the existing singleton*.
%
%      PRINCIPAL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PRINCIPAL.M with the given input arguments.
%
%      PRINCIPAL('Property','Value',...) creates a new PRINCIPAL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before principal_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to principal_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help principal

% Last Modified by GUIDE v2.5 13-Dec-2017 20:55:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @principal_OpeningFcn, ...
                   'gui_OutputFcn',  @principal_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before principal is made visible.
function principal_OpeningFcn(hObject, eventdata, handles, varargin)


% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to principal (see VARARGIN)

% Choose default command line output for principal
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes principal wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = principal_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in btnVerImagen.
function btnVerImagen_Callback(hObject, eventdata, handles)
img=imread('paisaje.jpg');
image(handles.axes1,img)
% --- Executes on button press in btnExaminarImagen.
function btnExaminarImagen_Callback(hObject, eventdata, handles)
global img;
global imagen;
[fileName path]=uigetfile({'*.png';'*.jpg'},'Abrir Documento');
imagen=[path fileName];
img=imread(strcat(path,fileName));
image(handles.axes1, img);
% --- Executes on button press in btnGuardarImagen.


% --- Executes on button press in btnProcesarImagen.
function btnProcesarImagen_Callback(hObject, eventdata, handles)
    global img;

    %% load dataset
    load('trainset.mat');
    load('className.mat');
    
    %% clasificación KNN
    model = fitcknn(trainset,className);
    model.NumNeighbors = 11;
    instance4test= getFeatures(img);
    prediccion = predict (model,instance4test);
    set(handles.txtResultadoProcesamiento,'string',prediccion);
    
    


function txtResultadoProcesamiento_Callback(hObject, eventdata, handles)
%
% --- Executes during object creation, after setting all properties.
function txtResultadoProcesamiento_CreateFcn(hObject, eventdata, handles)
% 
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes on button press in btnTexto.
function btnTexto_Callback(hObject, eventdata, handles)

global imagen;

    %% load dataset
    load('trainset.mat');
    load('className.mat');
    %% clasificación KNN
    model = fitcknn(trainset,className);
    model.NumNeighbors = 11;
    % se obtienen los segmentos de la imagen
    segments = getSegmentsFromImage(imagen,0);
    numObj = length(segments);
            
   for k = 1 : numObj      
        label = predictionGivenImage(model,segments(k).image);
        rectangle('Position', segments(k).bBox,'EdgeColor','r');
        text(segments(k).bBox(1) + segments(k).center(2),segments(k).bBox(2)+ segments(k).center(1),label,'Color','m','FontSize',20);
        set(handles.txtResultadoProcesamiento,'string',label);
   end  
